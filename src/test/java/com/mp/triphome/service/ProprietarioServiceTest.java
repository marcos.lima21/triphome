package com.mp.triphome.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mp.triphome.TripHomeApplicationTests;
import com.mp.triphome.db.model.entity.Proprietario;

public class ProprietarioServiceTest extends TripHomeApplicationTests {

	@Autowired
	private ProprietarioService proprietarioService;

	public ProprietarioServiceTest() {
		super("proprietarioService.xml");
	}

	@Test
	public void test01() {
		assertEquals(2, proprietarioService.buscarTodos().size());
	}

	@Test
	public void test02() {
		Proprietario salvo = proprietarioService.salvar(new Proprietario(3L, "PELE"));
		assertNotNull(salvo);
		assertTrue(proprietarioService.buscarPorId(salvo.getId()).isPresent());
		assertEquals("PELE", salvo.getNome());
	}

	@Test
	public void test03() {
		assertTrue(proprietarioService.buscarPorId(1L).isPresent());
		assertFalse(proprietarioService.buscarPorId(2L).isPresent());
		assertTrue(proprietarioService.buscarPorId(3L).isPresent());
	}

	@Test
	public void test04() {
		Proprietario antigo = proprietarioService.buscarPorId(1L).get();
		antigo.setNome("LIMA");

		Proprietario salvo = proprietarioService.salvar(antigo);
		assertNotNull(salvo);
		assertTrue(proprietarioService.buscarPorId(salvo.getId()).isPresent());
		assertEquals("LIMA", salvo.getNome());
	}
}
