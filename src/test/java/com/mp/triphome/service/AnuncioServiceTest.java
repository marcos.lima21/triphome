package com.mp.triphome.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mp.triphome.TripHomeApplicationTests;
import com.mp.triphome.db.model.entity.Anuncio;
import com.mp.triphome.db.model.entity.Endereco;
import com.mp.triphome.db.model.entity.Imovel;
import com.mp.triphome.db.model.enumaration.Status;
import com.mp.triphome.db.model.enumaration.TipoImovel;

public class AnuncioServiceTest extends TripHomeApplicationTests {

	@Autowired
	private AnuncioService anuncioService;

	@Autowired
	private ProprietarioService proprietarioService;

	public AnuncioServiceTest() {
		super("anuncioService.xml");
	}

	@Test
	public void test01() {
		assertEquals(0, anuncioService.buscarTodos().size());
	}

	@Test
	public void test02() {
		Endereco endereco = new Endereco();
		endereco.setPais("BRASIL");
		endereco.setRua("AV. Beira Mar");
		endereco.setCidade("Fortaleza");
		endereco.setUf("CE");
		endereco.setCep("330139");

		Imovel imovel = new Imovel();
		imovel.setNome("IMOVEL DE PRAIA");
		imovel.setDescricao("");
		imovel.setQuantidadeHospedes(1);
		imovel.setQuantidadeQuartos(2);
		imovel.setQuantidadeCamas(1);
		imovel.setQuantidadeBanheiros(1);
		imovel.setValor(BigDecimal.TEN);
		imovel.setTipoImovel(TipoImovel.CASA);
		imovel.setEndereco(endereco);
		
		List<String> urls = new ArrayList<>();
		urls.add("http:");
		urls.add("www:");
		imovel.setImagensImovel(urls);

		Anuncio anuncio = new Anuncio();
		anuncio.setImovel(imovel);
		anuncio.setProprietario(proprietarioService.buscarPorId(1L).get());

		assertNotNull(anuncioService.salvar(anuncio));
	}

	@Test
	public void test03() {
		Optional<Anuncio> anuncioOpt = anuncioService.buscarPorId(1L); 
		assertTrue(anuncioOpt.isPresent());
		assertNotNull(anuncioOpt.get().getImovel());
		assertNotNull(anuncioOpt.get().getProprietario());
		assertEquals(Status.INICIADO, anuncioOpt.get().getStatus());
	}
}
