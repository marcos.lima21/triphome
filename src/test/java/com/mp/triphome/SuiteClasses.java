package com.mp.triphome;

import java.io.File;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.mp.triphome.service.AnuncioServiceTest;
import com.mp.triphome.service.ProprietarioServiceTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({ 
	AnuncioServiceTest.class,
	ProprietarioServiceTest.class,
})
public class SuiteClasses {

	public static final String PATH_DATASET = System.getProperty("user.dir")
			+ "/src/test/resources/data-set/".replace("/", File.separator);
}
