package com.mp.triphome.config;

import java.io.File;
import java.io.FileInputStream;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

@Component
public class DBUnitHelper {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	private String xmlFolder;
	
	public DBUnitHelper() {
		this(System.getProperty("user.dir") + "/src/test/resources/data-set/".replace("/", File.separator));
	}
	
	public DBUnitHelper(String xmlFolder) {
		this.xmlFolder = xmlFolder;
	}

	public void execute(DatabaseOperation operation, String dataSetName) {
		try {
			
			IDatabaseConnection connection = new DatabaseConnection(dataSource.getConnection());
			DatabaseConfig dbConfig = connection.getConfig();
			dbConfig.setProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, true);
			FileInputStream fileInput = new FileInputStream(xmlFolder + dataSetName);
			operation.execute(connection, new FlatXmlDataSetBuilder().build(fileInput));
		
		} catch (Exception e) {
			throw new RuntimeException("Erro ao executar DbUnit", e);
		}
	}

}
