package com.mp.triphome.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan(basePackageClasses = TestConfig.class)
public class TestConfig {

	@Autowired
	protected Environment environment;

	@Bean
	public DataSource remoteDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(environment.getProperty("spring.datasource.driver-class-name"));
		dataSource.setUrl(environment.getProperty("spring.datasource.url"));
		dataSource.setPassword(environment.getProperty("spring.datasource.password"));
		dataSource.setUsername(environment.getProperty("spring.datasource.username"));
		return dataSource;
	}

}
