package com.mp.triphome.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mp.triphome.TripHomeApplication;
import com.mp.triphome.db.model.entity.Anuncio;
import com.mp.triphome.db.model.entity.Endereco;
import com.mp.triphome.db.model.entity.Imovel;
import com.mp.triphome.db.model.entity.Proprietario;
import com.mp.triphome.db.model.enumaration.TipoImovel;
import com.mp.triphome.service.AnuncioService;
import com.mp.triphome.service.ProprietarioService;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = TripHomeApplication.class)
public class AnuncioControllerTest {

	@Autowired
	private AnuncioService anuncioService;

	@Autowired
	private ProprietarioService proprietarioService;

	@Autowired
	private MockMvc mockMvc;

	@After
	public void db() {
		proprietarioService.excluirTodos();
		proprietarioService.salvar(new Proprietario("MARCOS PAULO"));
	}

	@Test
	public void test01() throws Exception {
		mockMvc.perform(get("/api/anuncios").contentType(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isNoContent());
	}

	@Test
	public void test02() throws IOException, Exception {
		Endereco endereco = new Endereco();
		endereco.setPais("BRASIL");
		endereco.setRua("AV. Beira Mar");
		endereco.setCidade("Fortaleza");
		endereco.setUf("CE");
		endereco.setCep("330139");

		Imovel imovel = new Imovel();
		imovel.setNome("Novo imovel na praia");
		imovel.setDescricao("");
		imovel.setQuantidadeHospedes(1);
		imovel.setQuantidadeQuartos(2);
		imovel.setQuantidadeCamas(1);
		imovel.setQuantidadeBanheiros(1);
		imovel.setValor(BigDecimal.TEN);
		imovel.setTipoImovel(TipoImovel.CASA);
		imovel.setEndereco(endereco);

		Anuncio anuncio = new Anuncio();
		anuncio.setImovel(imovel);

		mockMvc.perform(
				post("/api/add-anuncio").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(anuncio)));

		List<Anuncio> found = anuncioService.buscarTodos();
		assertThat(found).extracting(a -> a.getImovel().getNome()).containsOnly("Novo imovel na praia");
	}

	@Test
	public void test03() throws Exception {
		mockMvc.perform(get("/api/anuncios").contentType(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(1))))
				.andExpect(jsonPath("$[0].imovel.nome", is("Novo imovel na praia")));
	}
}