package com.mp.triphome;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.dbunit.DatabaseUnitException;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mp.triphome.config.DBUnitHelper;
import com.mp.triphome.config.TestConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { TripHomeApplication.class, TestConfig.class })
@TestPropertySource(locations="classpath:application.test.properties")
public abstract class TripHomeApplicationTests {

	private static Set<Class<? extends TripHomeApplicationTests>> initialized = new HashSet<>();

	@Autowired
	private DBUnitHelper helper;

	@Autowired
	public Environment environment;

	private String dataSetName;
	private String dataSetReset;

	public TripHomeApplicationTests() {

	}

	public TripHomeApplicationTests(String dataSetName) {
		this.dataSetReset = "resetAll.xml";
		this.dataSetName = dataSetName;
	}

	@Before
	public void beforeTest() throws IOException, DatabaseUnitException, SQLException {
		if (initialized.contains(this.getClass()) || dataSetName == null) {
			return;
		}

		initialized.add(this.getClass());

		helper.execute(DatabaseOperation.DELETE_ALL, dataSetReset);
		helper.execute(DatabaseOperation.INSERT, dataSetName);
	}
}
