select * from imagem_imovel

INSERT INTO proprietario (id, nome) VALUES (nextval('proprietario_id_seq'), 'MARCOS PAULO');

INSERT INTO imovel (
	id, 
	descricao, cep, cidade, complemento,
	pais, rua, uf,
	nome, observacao,
	quantidade_quartos, quantidade_banheiros, quantidade_camas, quantidade_hospedes,
	tipo_imovel, valor, comodidade_imovel_id
) VALUES (
	nextval('imovel_id_seq'),
	'', '33013399', 'Fortaleza', NULL, 
	'Brasil', 'AV. Beira Mar', 'CE',
	'IMOVEL DE PRAIA', NULL,
	2, 1, 1, 1, 
	1,'10.00',NULL
);

Insert into imovel_id (
	imovel_id, url, order_id
) values (
	1, 'https://www.transportal.com.br/noticias/wp-content/uploads/2019/11/Farol-da-Barra.jpg', 0
);

INSERT INTO anuncio (
	id,
	status,imovel_id,proprietario_id,reserva_id
)
VALUES (
	nextval('anuncio_id_seq'),
	1,1,1,NULL
);