package com.mp.triphome.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.mp.triphome.db.model.entity.Proprietario;
import com.mp.triphome.db.repository.ProprietarioRepository;

@Service
public class ProprietarioService {

	private ProprietarioRepository proprietarioRepository;

	public ProprietarioService(ProprietarioRepository proprietarioRepository) {
		this.proprietarioRepository = proprietarioRepository;
	}

	public Optional<Proprietario> buscarPorId(Long id) {
		return proprietarioRepository.findById(id);
	}

	public List<Proprietario> buscarTodos() {
		return proprietarioRepository.findAll();
	}

	public Proprietario salvar(Proprietario proprietario) {
    	return proprietarioRepository.save(proprietario);
    }
	
	public void excluirTodos() {
		proprietarioRepository.deleteAll();
	}
}
