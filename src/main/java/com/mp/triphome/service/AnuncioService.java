package com.mp.triphome.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.mp.triphome.db.model.entity.Anuncio;
import com.mp.triphome.db.repository.AnuncioRepository;

@Service
public class AnuncioService {

	private AnuncioRepository anuncioRepository;

	public AnuncioService(AnuncioRepository anucioRepository) {
		this.anuncioRepository = anucioRepository;
	}
	
	public Optional<Anuncio> buscarPorId(Long id) {
		return anuncioRepository.findById(id);
	}

	public List<Anuncio> buscarTodos() {
		return anuncioRepository.findAll();
	}

	public Anuncio salvar(Anuncio anuncio) {
    	return anuncioRepository.save(anuncio);
    }
}
