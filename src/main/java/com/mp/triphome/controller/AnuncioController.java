package com.mp.triphome.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mp.triphome.db.model.entity.Anuncio;
import com.mp.triphome.service.AnuncioService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/")
@CrossOrigin(origins = "http://localhost:4200")
public class AnuncioController {

	@Autowired
	AnuncioService anucioService;

	@ApiOperation(value = "Busca todos os anúncios")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Retorna uma lista de json anuncios",
		response = Anuncio.class), 
	})
	@GetMapping(path = "anuncios")
	public ResponseEntity<?> buscarTodos() {
		List<Anuncio> anuncios = anucioService.buscarTodos();
		if (!anuncios.isEmpty()) {			
			return ResponseEntity.ok(anuncios);
		}
		return ResponseEntity.noContent().build();
	}

	@ApiOperation(value = "Salvar o anúncio")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Salvar um anúncio",
		response = Anuncio.class), 
	})
	@PostMapping(path = "add-anuncio")
	public ResponseEntity<?> salvar(
			@ApiParam(name = "json", value = "Json com informacoes do anúncio", required = true)
			@RequestBody Anuncio anuncio) {

		if (anuncio != null) {
			return ResponseEntity.ok(anucioService.salvar(anuncio));
		}

		return ResponseEntity.noContent().build();
	}
}