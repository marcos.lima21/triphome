package com.mp.triphome.db.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.mp.triphome.db.model.enumaration.Status;

@Entity
public class Anuncio implements Serializable {

	private static final long serialVersionUID = 7660740399167052565L;

	@Id
	@SequenceGenerator(name = "ANUNCIO_SEQ", sequenceName = "anuncio_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ANUNCIO_SEQ")
	private Long id;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "imovel_id")
	private Imovel imovel;

	@ManyToOne
	@JoinColumn(name = "proprietario_id")
	private Proprietario proprietario;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "anucio_reserva", 
		joinColumns = @JoinColumn(name = "anuncio_id", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name = "reserva_id", referencedColumnName = "id"))
	private List<Reserva> reservas;

	@Enumerated(EnumType.ORDINAL)
	@Column(nullable = false)
	private Status status;

	public Anuncio() {
		this.reservas = new ArrayList<>();
		this.status = Status.INICIADO;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Imovel getImovel() {
		return imovel;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}

	public Proprietario getProprietario() {
		return proprietario;
	}

	public void setProprietario(Proprietario proprietario) {
		this.proprietario = proprietario;
	}

	public List<Reserva> getReservas() {
		return reservas;
	}

	public void setReservas(List<Reserva> reservas) {
		this.reservas = reservas;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (getId() != null ? getId().hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Anuncio)) {
			return false;
		}

		Anuncio other = (Anuncio) object;
		return (this.getId() == null && other.getId() != null)
				|| (this.getId() != null && !this.getId().equals(other.getId()));
	}

	@Override
	public String toString() {
		return "[ID=" + getId() + "]";
	}
}
