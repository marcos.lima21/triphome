package com.mp.triphome.db.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Proprietario implements Serializable {

	private static final long serialVersionUID = 5476529685306095957L;

	@Id
	@SequenceGenerator(name = "PROPRIETARIO_SEQ", sequenceName = "proprietario_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "PROPRIETARIO_SEQ")
	private Long id;

	@Column(length = 255, nullable = false)
	private String nome;

	public Proprietario() {

	}

	public Proprietario(String nome) {
		this.nome = nome;
	}

	public Proprietario(Long id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (getId() != null ? getId().hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Proprietario)) {
			return false;
		}

		Proprietario other = (Proprietario) object;
		return (this.getId() == null && other.getId() != null)
				|| (this.getId() != null && !this.getId().equals(other.getId()));
	}

	@Override
	public String toString() {
		return "[ID=" + getId() + "]";
	}
}
