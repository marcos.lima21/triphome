package com.mp.triphome.db.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity(name = "comodidade_imovel")
public class ComodidadeImovel implements Serializable {

	private static final long serialVersionUID = 4407454813532704705L;

	@Id
	@SequenceGenerator(name = "COMODIDADE_IMOVEL_SEQ", sequenceName = "comodidade_imovel_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "COMODIDADE_IMOVEL_SEQ")
	private Long id;

	@Column
	private boolean basico;

	@Column(name = "itens_basicos_cozinha")
	private boolean itensBasicosCozinha;

	@Column(name = "mesa_area_trabalho")
	private boolean mesaAreaTrabalho;

	@Column(name = "ferro_passar")
	private boolean ferroPassar;

	@Column(name = "secador_cabelo")
	private boolean secadorCabelo;

	@Column
	private boolean xampu;

	@Column
	private boolean cabides;

	@Column
	private boolean tv;

	@Column
	private boolean aquecedor;

	@Column(name = "wi_fi")
	private boolean wiFi;

	@Column(name = "cafe_manha")
	private boolean cafeManha;

	@Column(name = "ar_condicionado")
	private boolean arCondicionado;

	@Column(name = "entrada_privada")
	private boolean entradaPrivada;

	@Column(name = "detector_fumaca")
	private boolean detectorFumaca;

	@Column(name = "kit_primeiro_socorro")
	private boolean kitPrimeiroSocorro;

	@Column(name = "extintor_incendio")
	private boolean extintorIncendio;

	@Column(name = "tranca_porta_quarto")
	private boolean trancaPortaQuarto;

	@Column(name = "area_lavanderia_secadora")
	private boolean areaLavanderiaSecadora;

	@Column(name = "area_jacuzzi")
	private boolean areaJacuzzi;

	@Column(name = "area_cozinha")
	private boolean areaCozinha;

	@Column(name = "area_piscina")
	private boolean areaPiscina;

	@Column(name = "area_lavanderia_maquina_lavar")
	private boolean areaLavanderiaMaquinaLavar;

	@Column(name = "area_elevador")
	private boolean areaElevador;

	@Column(name = "area_estacionamento")
	private boolean areaEstacionamento;

	@Column(name = "area_academia")
	private boolean areaAcademia;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isBasico() {
		return basico;
	}

	public void setBasico(boolean basico) {
		this.basico = basico;
	}

	public boolean isItensBasicosCozinha() {
		return itensBasicosCozinha;
	}

	public void setItensBasicosCozinha(boolean itensBasicosCozinha) {
		this.itensBasicosCozinha = itensBasicosCozinha;
	}

	public boolean isMesaAreaTrabalho() {
		return mesaAreaTrabalho;
	}

	public void setMesaAreaTrabalho(boolean mesaAreaTrabalho) {
		this.mesaAreaTrabalho = mesaAreaTrabalho;
	}

	public boolean isFerroPassar() {
		return ferroPassar;
	}

	public void setFerroPassar(boolean ferroPassar) {
		this.ferroPassar = ferroPassar;
	}

	public boolean isSecadorCabelo() {
		return secadorCabelo;
	}

	public void setSecadorCabelo(boolean secadorCabelo) {
		this.secadorCabelo = secadorCabelo;
	}

	public boolean isXampu() {
		return xampu;
	}

	public void setXampu(boolean xampu) {
		this.xampu = xampu;
	}

	public boolean isCabides() {
		return cabides;
	}

	public void setCabides(boolean cabides) {
		this.cabides = cabides;
	}

	public boolean isTv() {
		return tv;
	}

	public void setTv(boolean tv) {
		this.tv = tv;
	}

	public boolean isAquecedor() {
		return aquecedor;
	}

	public void setAquecedor(boolean aquecedor) {
		this.aquecedor = aquecedor;
	}

	public boolean isWiFi() {
		return wiFi;
	}

	public void setWiFi(boolean wiFi) {
		this.wiFi = wiFi;
	}

	public boolean isCafeManha() {
		return cafeManha;
	}

	public void setCafeManha(boolean cafeManha) {
		this.cafeManha = cafeManha;
	}

	public boolean isArCondicionado() {
		return arCondicionado;
	}

	public void setArCondicionado(boolean arCondicionado) {
		this.arCondicionado = arCondicionado;
	}

	public boolean isEntradaPrivada() {
		return entradaPrivada;
	}

	public void setEntradaPrivada(boolean entradaPrivada) {
		this.entradaPrivada = entradaPrivada;
	}

	public boolean isDetectorFumaca() {
		return detectorFumaca;
	}

	public void setDetectorFumaca(boolean detectorFumaca) {
		this.detectorFumaca = detectorFumaca;
	}

	public boolean isKitPrimeiroSocorro() {
		return kitPrimeiroSocorro;
	}

	public void setKitPrimeiroSocorro(boolean kitPrimeiroSocorro) {
		this.kitPrimeiroSocorro = kitPrimeiroSocorro;
	}

	public boolean isExtintorIncendio() {
		return extintorIncendio;
	}

	public void setExtintorIncendio(boolean extintorIncendio) {
		this.extintorIncendio = extintorIncendio;
	}

	public boolean isTrancaPortaQuarto() {
		return trancaPortaQuarto;
	}

	public void setTrancaPortaQuarto(boolean trancaPortaQuarto) {
		this.trancaPortaQuarto = trancaPortaQuarto;
	}

	public boolean isAreaLavanderiaSecadora() {
		return areaLavanderiaSecadora;
	}

	public void setAreaLavanderiaSecadora(boolean areaLavanderiaSecadora) {
		this.areaLavanderiaSecadora = areaLavanderiaSecadora;
	}

	public boolean isAreaJacuzzi() {
		return areaJacuzzi;
	}

	public void setAreaJacuzzi(boolean areaJacuzzi) {
		this.areaJacuzzi = areaJacuzzi;
	}

	public boolean isAreaCozinha() {
		return areaCozinha;
	}

	public void setAreaCozinha(boolean areaCozinha) {
		this.areaCozinha = areaCozinha;
	}

	public boolean isAreaPiscina() {
		return areaPiscina;
	}

	public void setAreaPiscina(boolean areaPiscina) {
		this.areaPiscina = areaPiscina;
	}

	public boolean isAreaLavanderiaMaquinaLavar() {
		return areaLavanderiaMaquinaLavar;
	}

	public void setAreaLavanderiaMaquinaLavar(boolean areaLavanderiaMaquinaLavar) {
		this.areaLavanderiaMaquinaLavar = areaLavanderiaMaquinaLavar;
	}

	public boolean isAreaElevador() {
		return areaElevador;
	}

	public void setAreaElevador(boolean areaElevador) {
		this.areaElevador = areaElevador;
	}

	public boolean isAreaEstacionamento() {
		return areaEstacionamento;
	}

	public void setAreaEstacionamento(boolean areaEstacionamento) {
		this.areaEstacionamento = areaEstacionamento;
	}

	public boolean isAreaAcademia() {
		return areaAcademia;
	}

	public void setAreaAcademia(boolean areaAcademia) {
		this.areaAcademia = areaAcademia;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (getId() != null ? getId().hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof ComodidadeImovel)) {
			return false;
		}

		ComodidadeImovel other = (ComodidadeImovel) object;
		return (this.getId() == null && other.getId() != null)
				|| (this.getId() != null && !this.getId().equals(other.getId()));
	}

	@Override
	public String toString() {
		return "[ID=" + getId() + "]";
	}
}
