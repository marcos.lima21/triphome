package com.mp.triphome.db.model.enumaration;

public enum Status {

	INICIADO,
	ANDAMENTO,
	CONCLUIDO,
	CANCELADO
}
