package com.mp.triphome.db.model.enumaration;

public enum TipoImovel {

	APARTAMENTO,
	CASA,
	UNIDADE_SECUNDARIA,
	ACOMODACAO_UNICA,
	POUSA,
	HOTEL_BOUTIQUE
}
