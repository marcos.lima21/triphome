package com.mp.triphome.db.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.SequenceGenerator;

import com.mp.triphome.db.model.enumaration.TipoImovel;

@Entity
public class Imovel implements Serializable {

	private static final long serialVersionUID = -3448199921236036983L;

	@Id
	@SequenceGenerator(name = "IMOVEL_SEQ", sequenceName = "imovel_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "IMOVEL_SEQ")
	private Long id;

	@Column(length = 255, nullable = false)
	private String nome;

	@Embedded
	private Endereco endereco;

	@Column(length = 255, nullable = false)
	private String descricao;

	@Column(length = 255)
	private String observacao;
	
	@Column(name = "quantidade_hospedes", nullable = false)
	private Integer quantidadeHospedes;

	@Column(name = "quantidade_quartos", nullable = false)
	private Integer quantidadeQuartos;

	@Column(name = "quantidade_camas", nullable = false)
	private Integer quantidadeCamas;

	@Column(name = "quantidade_banheiros", nullable = false)
	private Integer quantidadeBanheiros;

	@Column(nullable = false)
	private BigDecimal valor;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "comodidade_imovel_id")
	private ComodidadeImovel comodidadeImovel;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "tipo_imovel", nullable = false)
	private TipoImovel tipoImovel;

	@ElementCollection
	@OrderColumn(name = "order_id")
	@JoinTable(name = "imagem_imovel", joinColumns = @JoinColumn(name = "imovel_id"))
	@Column(name = "url", nullable = false, length = 2000)
	private List<String> imagensImovel;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getQuantidadeHospedes() {
		return quantidadeHospedes;
	}

	public void setQuantidadeHospedes(Integer quantidadeHospedes) {
		this.quantidadeHospedes = quantidadeHospedes;
	}
	
	public Integer getQuantidadeQuartos() {
		return quantidadeQuartos;
	}

	public void setQuantidadeQuartos(Integer quantidadeQuartos) {
		this.quantidadeQuartos = quantidadeQuartos;
	}

	public Integer getQuantidadeCamas() {
		return quantidadeCamas;
	}

	public void setQuantidadeCamas(Integer quantidadeCamas) {
		this.quantidadeCamas = quantidadeCamas;
	}

	public Integer getQuantidadeBanheiros() {
		return quantidadeBanheiros;
	}

	public void setQuantidadeBanheiros(Integer quantidadeBanheiros) {
		this.quantidadeBanheiros = quantidadeBanheiros;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public ComodidadeImovel getComodidadeImovel() {
		return comodidadeImovel;
	}

	public void setComodidadeImovel(ComodidadeImovel comodidadeImovel) {
		this.comodidadeImovel = comodidadeImovel;
	}

	public TipoImovel getTipoImovel() {
		return tipoImovel;
	}

	public void setTipoImovel(TipoImovel tipoImovel) {
		this.tipoImovel = tipoImovel;
	}

	public List<String> getImagensImovel() {
		return imagensImovel;
	}

	public void setImagensImovel(List<String> imagensImovel) {
		this.imagensImovel = imagensImovel;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (getId() != null ? getId().hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof ComodidadeImovel)) {
			return false;
		}

		ComodidadeImovel other = (ComodidadeImovel) object;
		return (this.getId() == null && other.getId() != null)
				|| (this.getId() != null && !this.getId().equals(other.getId()));
	}

	@Override
	public String toString() {
		return "[ID=" + getId() + "]";
	}

}
