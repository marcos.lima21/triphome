package com.mp.triphome.db.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Reserva implements Serializable {

	private static final long serialVersionUID = 5768903021394611252L;

	@Id
	@SequenceGenerator(name = "RESERVA_SEQ", sequenceName = "reserva_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "RESERVA_SEQ")
	private Long id;

	@Column(name = "check_in", nullable = false)
	private Date checkin;

	@Column(name = "check_out", nullable = false)
	private Date checkout;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCheckin() {
		return checkin;
	}

	public void setCheckin(Date checkin) {
		this.checkin = checkin;
	}

	public Date getCheckout() {
		return checkout;
	}

	public void setCheckout(Date checkout) {
		this.checkout = checkout;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (getId() != null ? getId().hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Reserva)) {
			return false;
		}

		Reserva other = (Reserva) object;
		return (this.getId() == null && other.getId() != null)
				|| (this.getId() != null && !this.getId().equals(other.getId()));
	}

	@Override
	public String toString() {
		return "[ID=" + getId() + "]";
	}
}
