package com.mp.triphome.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mp.triphome.db.model.entity.Proprietario;

@RepositoryRestResource
public interface ProprietarioRepository extends JpaRepository<Proprietario, Long>, JpaSpecificationExecutor<Proprietario> {

}
